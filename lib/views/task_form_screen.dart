import 'package:HTTPDemo/blocs/task_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TaskFormScreen extends StatelessWidget {
  final TaskBloc taskBloc;

  TaskFormScreen({Key key, this.taskBloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController descriptionController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: Text('form'),
      ),
      body: Column(
        children: [
          Container(
              margin: EdgeInsets.all(20),
              child: TextFormField(
                controller: descriptionController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.description),
                  labelText: 'description',
                ),
              )),
          RaisedButton(
              child: Text('submit'),
              onPressed: () {
                taskBloc
                    .add(CreateTask(description: descriptionController.text));
                Navigator.pop(context, {'message': true});
              })
        ],
      ),
    );
  }
}
