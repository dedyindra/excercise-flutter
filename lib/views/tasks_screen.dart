import 'package:HTTPDemo/blocs/task_bloc.dart';
import 'package:HTTPDemo/models/task.dart';
import 'package:HTTPDemo/views/task_form_screen.dart';
import 'package:HTTPDemo/views/widgets/error.dart';
import 'package:HTTPDemo/views/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TaskScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<TaskScreen> {
  TaskBloc _bloc;
  bool message = false;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<TaskBloc>(context);
    _bloc.add(GetTask());
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  _callbackNavigation(TaskBloc taskBloc) async {
    var result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => TaskFormScreen(
                  taskBloc: taskBloc,
                )));
    Map decoded = result as Map;
    if (decoded != null) {
        if(decoded['message']){
          _bloc.add(GetTask());
        }
    }
  }

  @override
  Widget build(BuildContext context) => RefreshIndicator(
        onRefresh: () async {
          _bloc.add(GetTask());
        },
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                _callbackNavigation(_bloc);
              }),
          appBar: AppBar(
            title: Text('HTTP Calls Demo'),
          ),
          body: BlocConsumer<TaskBloc, TaskState>(
            listener: (context, state) {
              if (state is DeleteTaskSuccess ||
                  state is UpdateTaskSuccess ||
                  state is CreateTaskSuccess) {
                _bloc.add(GetTask());
              }
            },
            bloc: _bloc,
            builder: (context, state) {
              if (state is TaskLoading) {
                return LoaderComponent();
              }
              if (state is TaskLoaded) {
                return TaskList(tasks: state.task, taskBloc: _bloc);
              }
              if (state is TaskError) {
                return ErrorComponent(
                  errorMessage: 'error',
                  onRetryPressed: () => _bloc.add(GetTask()),
                );
              }
              return Container();
            },
          ),
        ),
      );
}

class TaskList extends StatelessWidget {
  final Task tasks;
  final TaskBloc taskBloc;

  const TaskList({Key key, this.tasks, this.taskBloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return InkWell(
            onTap: () {},
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 60,
                      child: Text(
                        tasks.data[index].description,
                      ),
                    ),
                    Text(
                      'status : ${tasks.data[index].completed.toString()}',
                    ),
                    Row(
                      children: [
                        IconButton(
                            icon: Icon(
                              Icons.check,
                              color: Colors.green,
                            ),
                            onPressed: () {
                              taskBloc
                                  .add(UpdateTask(id: tasks.data[index].sId));
                            }),
                        IconButton(
                            icon: Icon(
                              Icons.cancel,
                              color: Colors.red,
                            ),
                            onPressed: () {
                              taskBloc
                                  .add(DeleteTask(id: tasks.data[index].sId));
                            })
                      ],
                    )
                  ],
                ),
              ),
            ));
      },
      itemCount: tasks.data.length,
    );
  }
}
