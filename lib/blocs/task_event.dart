part of 'task_bloc.dart';

abstract class TaskEvent  {}
class GetTask extends TaskEvent {
  GetTask();

  List<Object> get props => [];
}

class CreateTask extends TaskEvent {
  final String description;
  CreateTask({this.description});

  List<Object> get props => [description];
}

class DeleteTask extends TaskEvent {
  final String id;
  DeleteTask({this.id});

  List<Object> get props => [id];
}

class UpdateTask extends TaskEvent {
  final String id;
  UpdateTask({this.id,});

  List<Object> get props => [id];
}