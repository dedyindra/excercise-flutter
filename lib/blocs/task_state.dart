
part of 'task_bloc.dart';

abstract class TaskState {
  const TaskState();

  List<Object> get props => [];
}

class TaskInitial extends TaskState {}
class TaskLoading extends TaskState {}
class TaskError extends TaskState {}

class TaskLoaded extends TaskState {
  final Task task;


  TaskLoaded({
    this.task
  });

  @override
  List<Object> get props => [

  ];
}

class CreateTaskSuccess extends TaskState {}
class DeleteTaskSuccess extends TaskState {}
class UpdateTaskSuccess extends TaskState {}
