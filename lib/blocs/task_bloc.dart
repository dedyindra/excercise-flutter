import 'dart:async';

import 'package:HTTPDemo/models/task.dart';
import 'package:HTTPDemo/repository/task_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'task_event.dart';

part 'task_state.dart';

class TaskBloc extends Bloc<TaskEvent, TaskState> {
  TaskRepository _taskRepository;
  TaskBloc() {
    _taskRepository = TaskRepository();

  }
  @override
  TaskState get initialState => TaskInitial();

  @override
  Stream<TaskState> mapEventToState(TaskEvent event) async* {
    if (event is GetTask) {
      yield TaskLoading();
      try {
        Task data = await _taskRepository.fetchTask() ;
        yield  TaskLoaded(task: data);
      } catch (e) {
        yield TaskError();
      }
    }

    if (event is CreateTask) {
      yield TaskLoading();
      try {
        Map<String ,dynamic> payload ={
          'description': event.description
    };
        await _taskRepository.createTask(payload) ;
        yield  CreateTaskSuccess();
      } catch (e) {
        yield TaskError();
      }
    }
    if (event is DeleteTask) {
      yield TaskLoading();
      try {
        await _taskRepository.deleteTask(event.id) ;
        yield  DeleteTaskSuccess();
      } catch (e) {
        yield TaskError();
      }
    }
    if (event is UpdateTask) {
      yield TaskLoading();
      try {
        Map<String ,dynamic> payload ={
          'completed': true
        };
        await _taskRepository.updateTask(event.id,payload) ;
        yield  UpdateTaskSuccess();
      } catch (e) {
        yield TaskError();
      }
    }





  }
}
