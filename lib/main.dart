import 'package:HTTPDemo/blocs/task_bloc.dart';
import 'package:HTTPDemo/views/task_form_screen.dart';
import 'package:HTTPDemo/views/tasks_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  MaterialApp build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        home:  BlocProvider<TaskBloc>(
              create: (context) => TaskBloc(), child: TaskScreen()),

      );
}
