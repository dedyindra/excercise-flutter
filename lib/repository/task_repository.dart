import 'dart:async';
import 'package:HTTPDemo/models/task.dart';
import 'package:HTTPDemo/networking/api_provider.dart';

class TaskRepository {
  ApiProvider _provider = ApiProvider();

  Future<Task> fetchTask() async {
    final response = await _provider.get('task');
    return Task.fromJson(response);
  }

  Future<void> createTask(Map payload) async {
    await _provider.post(url: 'task', payload: payload);
  }

  Future<void> deleteTask(String id) async {
    await _provider.delete(url: 'task/$id');
  }
  Future<void> updateTask(String id,Map payload) async {
    await _provider.put(url: 'task/$id',payload: payload);
  }
}
