import 'package:HTTPDemo/networking/custom_exceptions.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';
import 'dart:async';

class ApiProvider {
  final String _baseUrl = "https://api-nodejs-todolist.herokuapp.com/";
  final String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDU5ZjNiMGVlMGJmNTAwMTczMzMxYjEiLCJpYXQiOjE2MTY1MDc4NjV9.1lK6qmdulGwJ6btFZ8bFqr2Zsb8N83x6Ce6DQPnRQHw";

  Future<dynamic> get(String url) async {
    var responseJson;
    try {
      final response = await http.get(_baseUrl + url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
      responseJson = _response(response);
      print(responseJson);

    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<dynamic> post({String url, Map payload}) async {
    var responseJson;
    try {
      final response = await http.post(_baseUrl + url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },  body: jsonEncode(payload));
      responseJson = _response(response);
      print(responseJson);

    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<dynamic> delete({String url}) async {
    var responseJson;
    try {
      final response = await http.delete(_baseUrl + url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },  );
      responseJson = _response(response);
      print(responseJson);

    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<dynamic> put({String url, Map payload}) async {
    var responseJson;
    try {
      final response = await http.put(_baseUrl + url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },  body: jsonEncode(payload));
      responseJson = _response(response);
      print(responseJson);

    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
