import 'package:HTTPDemo/networking/api_provider.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../__mocks__/joke_response.dart';

class MockApiProvider extends Mock implements ApiProvider {}

void main() {
  MockApiProvider http;

  setUp(() {
    http = MockApiProvider();
  });

  tearDown(() {
    http = null;
  });

  test('should return result', () async {
    // Given
    final expected = mockJSONResponse;
    const String mockUrl = 'jokes/random?category=food';

    // When
    when(http.get(argThat(startsWith(mockUrl))))
        .thenAnswer((_) async => mockJSONResponse);
    final result = await http.get(mockUrl);

    // Then
    expect(result, expected);
  });
}
