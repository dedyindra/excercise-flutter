const mockJSONResponse = {
  "categories": ["food"],
  "created_at": "2020-01-05 13:42:19.576875",
  "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
  "id": "ixljkjuwsdwxnlmmjwqsfg",
  "updated_at": "2020-01-05 13:42:19.576875",
  "url": "https://api.chucknorris.io/jokes/ixljkjuwsdwxnlmmjwqsfg",
  "value":
      "With the rising cost of gasoline, Chuck Norris is beginning to worry about his drinking habit."
};
